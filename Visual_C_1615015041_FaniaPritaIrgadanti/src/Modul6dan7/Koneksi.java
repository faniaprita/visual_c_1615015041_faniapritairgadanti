/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modul6dan7;

import java.sql.*;
import javax.swing.JOptionPane;

public class Koneksi {
    
    private static Connection con;
    
    public static Connection getConnection(){
       try {
           con=DriverManager.getConnection(
           "jdbc:mysql://:3306/petshop","root","");
       }
       catch(SQLException f){
            JOptionPane.showMessageDialog(null,"Koneksi Gagal: "+f.getMessage());
       }
       return con;
     }
    }
